/*
 * MOP Copyright (C) 2023 UTE Corporation
 *
 * ForzamientoControles is part of MOP.
 *
 * MOP is free software: you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * MOP is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with MOP. If
 * not, see <https://www.gnu.org/licenses/>.
 */

package interfacesParticipantes;


/**
 * En esta interfaz se definien mótodos para forzar los controles. Los participantes que implementen la interfaz probablemente tomen un conjunto
 * de pares (VariableControl x, Valor v) y agreguen restricciones mediante su comportamiento despacho de la forma x = v
 */
public interface ForzamientoControles {
	
}
