/*
 * MOP Copyright (C) 2023 UTE Corporation
 *
 * AgregadorVARaPVA is part of MOP.
 *
 * MOP is free software: you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * MOP is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with MOP. If
 * not, see <https://www.gnu.org/licenses/>.
 */

package procesosEstocasticos;

/**
 * Tiene los datos que permiten pasar de las variables de estado de un VAR en variables
 * de estado normalizadas a la variables de estado de un PVA, cuando ambos VAR y PVA se
 * han estimado en la misma Estimacion.
 * @author ut469262
 *
 */
public class AgregadorVARaPVA {
	
//	kk aca debería ir un atributo que permita acceder al CalculadorVE 
//	que permitió construir el PVA
//	
//	public double[] devuelveEstadoOptim(double[] valoresVESimul, double[] valoresVExo) {
//		Y ESTO COMO SE IMPLEMENTA?
//	}

}
