/*
 * MOP Copyright (C) 2023 UTE Corporation
 *
 * DatosEstimadoresMarkov is part of MOP.
 *
 * MOP is free software: you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * MOP is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with MOP. If
 * not, see <https://www.gnu.org/licenses/>.
 */

package interfazEstimadoresPE;
//
//import datatypes.DatosVariableAleatoria;
//import tiempo.Evolucion;
//
///**
// * Datatype que representa los datos de un aerogenerador eólico
// * @author ut602614
// *
// */
//
public class DatosEstimadoresMarkov {
//
//	private String nombre;
//
//
//
//	public DatosEstimadoresMarkov(String nombre, String barra,
//								  Evolucion<Integer> cantModInst, Evolucion<Double> potMin,
//								  Evolucion<Double> potMax, DatosVariableAleatoria factor,
//								  Integer cantModIni, Evolucion<Double> dispMedia,
//								  Evolucion<Double> tMedioArreglo, boolean salDetallada, Evolucion<Integer> mantProgramado, Evolucion<Double> costoFijo, Evolucion<Double> costoVariable) {
//		super();
//		this.nombre = nombre;
//	}
//
//
//
//	public Evolucion<Integer> getCantModInst() {
//		return cantModInst;
//	}
//
//
//
//	public void setCantModInst(Evolucion<Integer> cantModInst) {
//		this.cantModInst = cantModInst;
//	}
//
//
//
//	public String getNombre() {
//		return nombre;
//	}
//	public void setNombre(String nombre) {
//		this.nombre = nombre;
//	}
//	public String getBarra() {
//		return barra;
//	}
//	public void setBarra(String barra) {
//		this.barra = barra;
//	}
//
//	public Evolucion<Double> getPotMin() {
//		return potMin;
//	}
//	public void setPotMin(Evolucion<Double> potMin) {
//		this.potMin = potMin;
//	}
//	public Evolucion<Double> getPotMax() {
//		return potMax;
//	}
//	public void setPotMax(Evolucion<Double> potMax) {
//		this.potMax = potMax;
//	}
//	public Integer getCantModIni() {
//		return cantModIni;
//	}
//	public void setCantModIni(Integer cantModIni) {
//		this.cantModIni = cantModIni;
//	}
//
//	public Evolucion<Double> getDispMedia() {
//		return dispMedia;
//	}
//	public void setDispMedia(Evolucion<Double> dispMedia) {
//		this.dispMedia = dispMedia;
//	}
//	public Evolucion<Double> gettMedioArreglo() {
//		return tMedioArreglo;
//	}
//	public void settMedioArreglo(Evolucion<Double> tMedioArreglo) {
//		this.tMedioArreglo = tMedioArreglo;
//	}
//
//
//
//	public DatosVariableAleatoria getFactor() {
//		return factor;
//	}
//
//
//
//	public void setFactor(DatosVariableAleatoria factor) {
//		this.factor = factor;
//	}
//
//
//
//	public boolean isSalDetallada() {
//		return salDetallada;
//	}
//
//
//
//	public void setSalDetallada(boolean salDetallada) {
//		this.salDetallada = salDetallada;
//	}
//
//
//
//	public Evolucion<Integer> getMantProgramado() {
//		return mantProgramado;
//	}
//
//
//
//	public void setMantProgramado(Evolucion<Integer> mantProgramado) {
//		this.mantProgramado = mantProgramado;
//	}
//
//
//
//	public Evolucion<Double> getCostoFijo() {
//		return costoFijo;
//	}
//
//
//
//	public void setCostoFijo(Evolucion<Double> costoFijo) {
//		this.costoFijo = costoFijo;
//	}
//
//
//
//	public Evolucion<Double> getCostoVariable() {
//		return costoVariable;
//	}
//
//
//
//	public void setCostoVariable(Evolucion<Double> costoVariable) {
//		this.costoVariable = costoVariable;
//	}
//
}
//
